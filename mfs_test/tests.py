from django.test import TestCase
from .models import AppTest
# Create your tests here.


class AppTestCase(TestCase):
    def testApp(self):
        app_test = AppTest(points="(2,1), (1,1), (5,4)", result=1.0)
        self.assertEqual(app_test.points, "(2,1), (1,1), (5,4)")
        self.assertEqual(app_test.result, 1.0)
