from django.db import models

# Create your models here.


class AppTest(models.Model):
    points = models.CharField(max_length=1000, null=False, blank=False)
    result = models.FloatField(null=False, blank=False)

    def __str__(self):
        return self.points
