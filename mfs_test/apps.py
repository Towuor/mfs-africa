from django.apps import AppConfig


class MfsTestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mfs_test'
