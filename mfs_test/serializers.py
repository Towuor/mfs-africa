from rest_framework import serializers
from .models import AppTest


class StringSerializer(serializers.ModelSerializer):

    class Meta:
        model = AppTest
        fields = ['points', 'result', ]

        extra_kwargs = {'result': {'read_only': True}}
