from django.urls import path
from . import views

urlpatterns = [
    path('api/v1/test', views.MfsApi.as_view(), name="mfs_api")
]