from django.shortcuts import render
from .models import AppTest
from .serializers import StringSerializer
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import BaseAuthentication, SessionAuthentication
from django.views.decorators.csrf import csrf_exempt
from rest_framework.permissions import AllowAny

import math
import copy


# Create your views here.
class CsrfExemptSessionAuthentication(SessionAuthentication):

    def enforce_csrf(self, request):
        return


class ApiAuthentication(BaseAuthentication):

    def authenticate(self, username=None, password=None, token=None):
        return


# A class to represent a Point in 2D plane
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


# A utility function to find the
# distance between two points
# using the formula |pq| =  sqrt((px-qx)^2 + (py-qy)^2)
def dist(p1, p2):
    return math.sqrt((p1.x - p2.x) *
                     (p1.x - p2.x) +
                     (p1.y - p2.y) *
                     (p1.y - p2.y))


# A Brute Force method to return the
# smallest distance between two points
# in P[] of size n
# float('inf') as min_value is for creating an unbounded upper value for comparison,
#  a +ve infinite value
def brute_force(p, n):
    min_val = float('inf')
    for i in range(n):
        for j in range(i + 1, n):
            if dist(p[i], p[j]) < min_val:
                min_val = dist(p[i], p[j])

    return min_val


# A utility function to find the
# distance beween the closest points of
# strip of given size. All points in
# strip[] are sorted according to
# y coordinate.
def strip_closest(strip, size, d):
    # Initialize the minimum distance as d
    min_val = d

    # Pick all points one by one and
    # try the next points till the difference
    # between y coordinates is smaller than d.
    for i in range(size):
        j = i + 1
        while j < size and (strip[j].y - strip[i].y) < min_val:
            min_val = dist(strip[i], strip[j])
            j += 1

    return min_val


# A recursive function to find the
# smallest distance. The array P contains
# all points sorted according to x coordinate
def closest_util(p, q, n):
    # If there are 2 or 3 points,
    # then use brute force
    if n <= 3:
        return brute_force(p, n)

    # Find the middle point as a whole number
    mid = n // 2
    mid_point = p[mid]

    # Consider the vertical line passing
    # through the middle point calculate
    # the smallest distance dl on left
    # of middle point and dr on right side
    dl = closest_util(p[:mid], q, mid)
    dr = closest_util(p[mid:], q, n - mid)

    # Find the smaller of two distances
    d = min(dl, dr)

    # Build an array strip[] that contains
    # points close (closer than d)
    # to the line passing through the middle point
    strip = []
    for i in range(n):
        if abs(q[i].x - mid_point.x) < d:
            strip.append(q[i])

        # Find the closest points in strip.
    # Return the minimum of d and closest
    # distance is strip[]
    return min(d, strip_closest(strip, len(strip), d))


# The main function that finds
# the smallest distance.
# This method mainly uses closestUtil()
def closest(p, n):
    p.sort(key=lambda point: point.x)
    q = copy.deepcopy(p)
    q.sort(key=lambda point: point.y)

    # Use recursive function closestUtil()
    # to find the smallest distance
    return closest_util(p, q, n)


# POST Api to accept user input
class MfsApi(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, ApiAuthentication,)
    permission_classes = (AllowAny, )

    @csrf_exempt
    def post(self, request):
        serializer = StringSerializer(data=request.data)
        numbers = request.data.get('points')
        if serializer.is_valid():
            res = [idx for idx in eval(numbers) if isinstance(idx, tuple)]
            data = []
            for r in res:
                x, y = r
                data.append(Point(x, y))

            p = data
            n = len(p)
            print("The smallest distance is", closest(p, n))

            serializer.save(result=closest(p, n))
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)